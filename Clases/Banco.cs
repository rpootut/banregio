﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Banregio.Clases
{
    class Banco
    {
        //public Cliente cliente1 { get; set; }
        //public Cliente cliente2 { get; set; }
        //public Cliente cliente3 { get; set; }

        public List<Cliente> clientes;

        public Banco()
        {
            //cliente1 = new Cliente();
            //cliente2 = new Cliente();
            //cliente3 = new Cliente();

            clientes = new List<Cliente>();
        }

        public void AgregarCliente()
        {
            Cliente cliente = new Cliente();
            Console.Write("Nombre de cliente: ");
            cliente.Nombre = Console.ReadLine();
            Console.Write("Escribe el monto de apertura: ");
            try
            {
                double cantidad = double.Parse(Console.ReadLine());
                cliente.Depositar(cantidad);
                clientes.Add(cliente);
                Console.WriteLine("Cliente creado exitosamente :3");
            }
            catch (Exception e)
            {
                Console.WriteLine("Error creando cliente");
                AgregarCliente();
            }

        }

        public double Monto
        {
            get
            {
                //double monto = cliente1.Monto + cliente2.Monto + cliente3.Monto;
                double monto = 0;

                foreach (Cliente cliente in clientes)
                {
                    monto = monto + cliente.Monto;
                }

                return monto;
            }
        }

        public void Procesar()
        {
            //cliente1.Nombre = "David";
            //cliente2.Nombre = "AdonisJS";
            //cliente3.Nombre = "Paizon XD";

            //cliente1.Depositar(100);
            //cliente2.Depositar(200);
            //cliente3.Depositar(300);

            //AgregarCliente();
            //AgregarCliente();
            //AgregarCliente();

            while (true)
            {
                Console.WriteLine("Opciones: ");
                Console.WriteLine("1) Agregar cliente");
                Console.WriteLine("2) Hacer corte");

                string opcion = Console.ReadLine();
                if (opcion == "1")
                {
                    AgregarCliente();
                }
                else if (opcion == "2")
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Opción invalida :3");
                }
            }

            Console.WriteLine("El monto total es: " + Monto);
        }
    }
}
